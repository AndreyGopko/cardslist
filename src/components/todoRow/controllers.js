export default class TodoRowController {
  constructor($http, API_URL) {
    'ngInject';
    this.$http = $http;
    this.API_URL = API_URL;
  }
  $onInit() {
  }
  addCard() {
    console.log(this.row);
    this.$http.post(`${this.API_URL}/groups/${this.row._id}/cards`)
    .then(data => {
      console.log(data.data);
      this.row.cards = data.data;
    });
  }
}
